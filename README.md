# ListCat Android

Listar uma raças de gatos e ao selecionar um time mostrar os detalhes deste item

## Getting Started

Esse projeto usar o sistema de compilação Gradle. Para usar o projeto importe o projeto no Android Studio

```
https://gitlab.com/tosin/listcat.git
```
### Prerequisites

- Android SDK 28
- Android Build Tools V28.+
- Android X

## Built With

* [Retrofit](https://github.com/square/retrofit) - Type-safe HTTP client for Android and Java by Square, Inc.
* [Coroutines]()
* [Navigation]()
* [Glide](https://github.com/bumptech/glide) - 
<!-- * [DBFlow](https://github.com/Raizlabs/DBFlow) - -->


## Contributing 

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [Git](https://git-scm.com/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/listcat/android/tags). 

## Authors

* **Roger Tosin** - *Initial work* - [TosinRoger](https://gitlab.com/tosin)

See also the list of [contributors](https://gitlab.com/tosin/listcat/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
