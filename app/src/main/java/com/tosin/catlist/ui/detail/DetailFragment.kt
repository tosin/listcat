package com.tosin.catlist.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.tosin.catlist.R
import com.tosin.catlist.model.Breed
import com.tosin.catlist.model.CatBreed
import kotlinx.android.synthetic.main.detail_fields.*
import kotlinx.android.synthetic.main.fragment_details.*

class DetailFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val catBreed = arguments?.getSerializable("cat_breed_details") as? CatBreed

        catBreed?.let {
            setData(it)
        }
    }

    private fun setData(catBreed: CatBreed) {
        val options = RequestOptions()
            .centerCrop()
            .placeholder(R.drawable.ic_pets_black_24dp)
            .error(R.drawable.ic_pets_black_24dp)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)

        Glide.with(context!!)
            .load(catBreed.imageUrl)
            .apply(options)
            .into(imageView_detail)

        if (catBreed.breeds.isNotEmpty())
            firstBreed(catBreed.breeds.first())
        else
            noDataFound()

    }

    private fun noDataFound() {
        textView_detail_catBreed.text = "No data found"

        layout_detail_fields.visibility = View.GONE
    }

    private fun firstBreed(breed: Breed) {
        textView_detail_catBreed.text = breed.name
        textView_detail_origin.text = breed.origin
        textView_detail_description.text = breed.description
        textView_detail_urlWIki.text = breed.wikipediaUrl
        textView_detail_alterNames.text = if (breed.altNames.isEmpty()) " - " else breed.altNames
        textView_detail_life.text = String.format("%s years", breed.lifeSpan)
        textView_detail_weight.text = String.format("%s inches", breed.weight.imperial)
        textView_detail_temperament.text = breed.temperament

        ratingBar_detail_childFriendly.rating = breed.childFriendly.toFloat()
        ratingBar_detail_dogFriendly.rating = breed.dogFriendly.toFloat()
    }
}
