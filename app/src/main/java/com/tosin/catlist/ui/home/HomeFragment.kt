package com.tosin.catlist.ui.home

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.tosin.catlist.R
import com.tosin.catlist.model.view.ShowComponentUI
import com.tosin.catlist.ui.MainActivity
import com.tosin.catlist.ui.home.adapter.HomeAdapter
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    private lateinit var viewModel: HomeViewModel
    private lateinit var mAdapter: HomeAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)

        viewModel.catBreed
            .observe(this, Observer {
                it?.let { hashSet ->
                    mAdapter.addItemList(hashSet.toList())
                }
            })

        viewModel.showComponentUi
            .observe(this, Observer {
                it?.let { show ->
                    showSomeLayout(show)
                }
            })

        viewModel.errorMsg
            .observe(this, Observer {
                textView_home_error.text = it
            })

        context?.let {
            viewModel.fetchLastPage(it)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as? MainActivity)?.setSupportActionBar(toolbar_home)

        toolbar_home.title = getString(R.string.app_name)

        mAdapter = HomeAdapter {
            val args = Bundle()
            args.putSerializable("cat_breed_details", it)
            findNavController().navigate(R.id.select_item_show_details, args)
        }

        recyclerView_home.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(view.context)
            adapter = mAdapter
        }

        swipe_refresh_home.setOnRefreshListener {
            viewModel.fetchLastPage(view.context)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        return inflater.inflate(R.menu.about_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_about -> {
                findNavController().navigate(R.id.aboutFragment)
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun showSomeLayout(show: ShowComponentUI) {
        when (show) {
            ShowComponentUI.LIST -> {
                swipe_refresh_home.isRefreshing = false
                textView_home_error.visibility = View.GONE
            }
            ShowComponentUI.ERROR -> {
                swipe_refresh_home.isRefreshing = false
                textView_home_error.visibility = View.VISIBLE
            }
            ShowComponentUI.LOADING -> {
                swipe_refresh_home.isRefreshing = true
                textView_home_error.visibility = View.GONE
            }
            ShowComponentUI.TRY_AGAIN -> {
                swipe_refresh_home.isRefreshing = false
                textView_home_error.visibility = View.VISIBLE
            }
        }
    }
}
