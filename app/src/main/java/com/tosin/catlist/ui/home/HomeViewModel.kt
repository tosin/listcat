package com.tosin.catlist.ui.home

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tosin.catlist.model.CatBreed
import com.tosin.catlist.model.view.ShowComponentUI
import com.tosin.catlist.utils.VerifyNetwork
import kotlinx.coroutines.*

class HomeViewModel : ViewModel() {

    private var _catBreeds = MutableLiveData<HashSet<CatBreed>>()
    val catBreed: LiveData<HashSet<CatBreed>>
        get() = _catBreeds

    private var _errorMsg = MutableLiveData<String>()
    val errorMsg: LiveData<String>
        get() = _errorMsg

    val showComponentUi = MutableLiveData<ShowComponentUI>()

    private val job = Job()
    private val uiScope = CoroutineScope(job + Dispatchers.Main)
    private val repository = HomeRepository()

    fun fetchLastPage(context: Context) {
        showComponentUi.value = ShowComponentUI.LOADING

        if (VerifyNetwork.isNetworkOnline(context)) {
            requestList()
        }
        else {
            _errorMsg.value = "Sem conexão com a internet"
            showComponentUi.value = ShowComponentUI.ERROR
        }
    }

    private fun requestList() {
        uiScope.launch {
            val response = withContext(Dispatchers.IO) {
                repository.requestListFromPage(1)
            }
            withContext(Dispatchers.Main) {
                if (response.objectResponse is HashSet<*>) {
                    val hashSet = response.objectResponse as HashSet<CatBreed>
                    _catBreeds.value = hashSet
                    showComponentUi.value = ShowComponentUI.LIST
                }
                else {
                    _errorMsg.value = response.errorMessage
                    showComponentUi.value = ShowComponentUI.ERROR
                }
            }
        }
    }
}