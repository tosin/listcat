package com.tosin.catlist.ui.about

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import com.tosin.catlist.R
import com.tosin.catlist.ui.MainActivity
import kotlinx.android.synthetic.main.fragment_about.*

class AboutFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as? MainActivity)?.setSupportActionBar(toolbar_about)
        (requireActivity() as? MainActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (requireActivity() as? MainActivity)?.supportActionBar?.setDisplayShowHomeEnabled(true)

        toolbar_about.title = "About"
        toolbar_about.subtitle = getString(R.string.app_name)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            android.R.id.home -> {
                requireActivity().onBackPressed()
                true
            }
            else ->
                super.onOptionsItemSelected(item)
        }

    }

}
