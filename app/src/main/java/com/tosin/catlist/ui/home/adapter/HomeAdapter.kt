package com.tosin.catlist.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.tosin.catlist.R
import com.tosin.catlist.model.CatBreed
import com.tosin.catlist.utils.onItemClicked

class HomeAdapter(val listerner: onItemClicked<CatBreed>) : RecyclerView.Adapter<HomeViewHolder>() {

    private var list = mutableListOf<CatBreed>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_home, parent, false)
        return HomeViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        val catBreed = list[position]

        val options = RequestOptions()
            .centerCrop()
            .placeholder(R.drawable.ic_pets_black_24dp)
            .error(R.drawable.ic_pets_black_24dp)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)

        Glide.with(holder.itemView.context)
            .load(catBreed.imageUrl)
            .apply(options)
            .into(holder.image)

        val breed = if (catBreed.breeds.isEmpty())
            "Meow, não vou ter dizer quem eu sou!"
        else
            catBreed.breeds.first().name

        holder.breed.text = breed

        holder.itemView.setOnClickListener {
            listerner(catBreed)
        }
    }

    fun addItemList(list: List<CatBreed>) {
        val lastOld = this.list.size

        this.list.addAll(list)
        notifyItemRangeInserted(lastOld, this.list.size)
    }

}