package com.tosin.catlist.ui.home.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_home.view.*

class HomeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val image = itemView.imageView_itemHome!!
    val breed = itemView.textView_itemHome!!
}