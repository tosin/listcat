package com.tosin.catlist.ui.home

import com.tosin.catlist.model.response.ResponseApiRequest
import com.tosin.catlist.repository.BaseRetrofit
import com.tosin.catlist.repository.interfaces.CatRepository

class HomeRepository {

    suspend fun requestListFromPage(page: Int): ResponseApiRequest<HashSet<*>> {
        val baseRetrofit = BaseRetrofit.createService(CatRepository::class.java)
        val call = baseRetrofit.getCatListAsync(page = page)
        val temp = call.await()

        val aux = try {
            temp.body()
        } catch (e: Exception) {
            e
        }

        return if (aux is HashSet<*>)
            ResponseApiRequest(aux, null)
        else
            ResponseApiRequest(null, (aux as? Exception)?.localizedMessage)
    }
}