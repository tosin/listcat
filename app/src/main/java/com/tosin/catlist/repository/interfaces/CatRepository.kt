package com.tosin.catlist.repository.interfaces

import com.tosin.catlist.model.CatBreed
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CatRepository {
    @GET("v1/images/search")
    fun getCatListAsync(
        @Query("limit") limit:Int = 100,
        @Query("page") page: Int
    ) : Deferred<Response<HashSet<CatBreed>>>
}