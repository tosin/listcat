package com.tosin.catlist.model.view

enum class ShowComponentUI {
    LOADING, LIST, ERROR, TRY_AGAIN
}