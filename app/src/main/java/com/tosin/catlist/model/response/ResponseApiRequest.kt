package com.tosin.catlist.model.response

data class ResponseApiRequest<T> (
    val objectResponse: T?,
    val errorMessage: String?
)