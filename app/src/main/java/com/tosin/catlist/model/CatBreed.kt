package com.tosin.catlist.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CatBreed(
    @SerializedName("breeds")
    val breeds: List<Breed>,
    @SerializedName("id")
    val id: String,
    @SerializedName("url")
    val imageUrl: String
) : Serializable