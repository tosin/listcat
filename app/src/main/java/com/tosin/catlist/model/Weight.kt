package com.tosin.catlist.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Weight(
    @SerializedName("imperial")
    val imperial: String,
    @SerializedName("metric")
    val metric: String
) : Serializable