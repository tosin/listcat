package com.tosin.catlist

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import java.util.*

class AppApplication : Application() {
    override fun onCreate() {
        super.onCreate()


        if (Calendar.getInstance().get(Calendar.HOUR_OF_DAY) in 7..17)
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        else
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
    }
}