package com.tosin.catlist.utils

typealias onItemClicked<T> = (item: T) -> Unit