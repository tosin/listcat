package com.tosin.catlist.repository

import com.tosin.catlist.model.CatBreed
import com.tosin.catlist.repository.interfaces.CatRepository
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test

class RequestTest {

    @Test
    fun tryRequest() {
        val service = BaseRetrofit.createService(CatRepository::class.java)

        val call = service.getCatListAsync(page = 1)

        val result = runBlocking {
            call.await()
        }

        assert(result.isSuccessful)

        val obj = result.body()
        assert(obj is HashSet<CatBreed>)
        assert(obj != null)
        assert(obj!!.isNotEmpty())

    }
}